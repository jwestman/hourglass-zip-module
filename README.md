# hourglass-zip-module

Precompiled Godot binaries with the Zip Module patch, for use in Hourglass's CI.

See <https://github.com/godotengine/godot/pull/34444> for the upstream (Godot 4)
version of the patch.
